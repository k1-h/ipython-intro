## Introduction to IPython

#### Keyvan Hedayati

===
### Features

* Tab completion
* Rich History
* Introspection
* Shell, !<cmd>, var = !<cmd> to capture output
* Embed into running code
* [Parallel Computing](http://ipython.org/ipython-doc/stable/parallel/index.html)

---
### Magic commands

* %lsmagic, List all magic commands
* %quickref, Quick reference, [Online version](https://damontallen.github.io/IPython-quick-ref-sheets/)
* %cpaste, Paste & execute a pre-formatted code block from clipboard
* %load, Load code into the current frontend
* %save, Save a set of lines or a macro to a given filename
* %macro, Define a macro for future re-execution. It accepts ranges of history, filenames or string objects

---
### IPython QtConsole & Notebook

* [A gallery of interesting IPython Notebooks](https://github.com/ipython/ipython/wiki/A-gallery-of-interesting-IPython-Notebooks)
* [Python Descriptors Demystified](http://nbviewer.ipython.org/gist/ChrisBeaumont/5758381/descriptor_writeup.ipynb)
* [A collection of not-so-obvious Python stuff you should know!](http://nbviewer.ipython.org/github/rasbt/python_reference/blob/master/tutorials/not_so_obvious_python_stuff.ipynb?create=1)
* [A beginner's guide to Python's namespaces, scope resolution, and the LEGB rule](http://nbviewer.ipython.org/github/rasbt/python_reference/blob/master/tutorials/scope_resolution_legb_rule.ipynb?create=1)
* [Pandas Example](http://nbviewer.ipython.org/gist/twiecki/3962843)

---
### [IPython extensions](http://ipython.org/ipython-doc/dev/config/extensions/index.html)

* [Extensions Index](https://github.com/ipython/ipython/wiki/Extensions-Index)
* [autoreload](http://ipython.org/ipython-doc/dev/config/extensions/autoreload.html), reloads modules automatically before entering the execution of code
* [storemagic](http://ipython.org/ipython-doc/dev/config/extensions/storemagic.html), Stores variables, aliases and macros in IPython’s database.
* [CSV Magic](https://github.com/FrankSalad/ipython-csvmagic), Tools for quickly importing and exporting data from CSV files
* [Django ORM magic](https://github.com/FrankSalad/ipython-csvmagic), Define your django models in a cell and use them on the fly. Let the magic do the boring part.
* [ipythonPexpect magic](http://nbviewer.ipython.org/url/home.fnal.gov/~lyon/ipython_ext/ipythonPexpect_example.ipynb), A magic that allows IPython notebooks to interface with other programs via Pexpect


---

* [%hierarchy magics](https://github.com/tkf/ipython-hierarchymagic), The %hierarchy magic command draws an inheritance diagram of the given class or object. With the %%dot cell magic, you can write graphiz dot language in a cell.
* [pep8](https://github.com/SiggyF/notebooks), Allows to check for the pep8 styleguide using the cellmagic %%pep8.
* [duster](https://github.com/lebedov/duster), Reset namespace and automatically (re)load several modules immediately thereafter
* [grasp](https://pypi.python.org/pypi/grasp/0.3.2), A set of python functions to help with interactive object inspection and discovery.
* [db.py plugin](https://github.com/dongweiming/idb), ipython db.py shell extension
