import sqlite3
from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label


class MainApp(App):
    def build(self):
        return HelloWorld()


class HelloWorld(GridLayout):
    def __init__(self, **kwargs):
        super(HelloWorld, self).__init__(**kwargs)
        db = sqlite3.connect('db.sqlite')
        company_info = db.execute('SELECT * FROM companies').fetchall()
        self.cols = 8
        for company in company_info:
            self.add_widget(Label(text='Name: '))
            self.add_widget(Label(text=company[0]))
            self.add_widget(Label(text='Founded Year: '))
            self.add_widget(Label(text=company[1]))
            self.add_widget(Label(text='CEO: '))
            self.add_widget(Label(text=company[2]))
            self.add_widget(Label(text='Employees: '))
            self.add_widget(Label(text=company[3]))

MainApp().run()
